<?php
namespace Epsilo\Crawler;
use GuzzleHttp\Client;
use Protobuf\Identity\Crawler\Core\JobRequest;
use Protobuf\Identity\Crawler\Core\JobResponse;

class ShopeeChannel
{
    const KWBD_KEYWORD_PERFORMANCE = 'kwbd_keyword_performance';
    const API_GET_KWBD_KEYWORD_PERF = 'api/marketing/v3/pas/report/search_report_by_time/?start_time={start_time}&end_time={end_time}&agg_interval=1&SPC_CDS=451d06e2-80b8-45e6-a58a-26ff99aee44c&SPC_CDS_VER=2';

    function __construct()
    {

    }

    /**
     * @param JobRequest $jobRequest
     * @return JobResponse
     */
    public function GetData(JobRequest $jobRequest): JobResponse
    {

        $jobResponse = null;

        switch ($jobRequest->getDataPoint()) {
            case self::KWBD_KEYWORD_PERFORMANCE:
                $jobResponse = $this->getKeywordBiddingKeywordPerformance($jobRequest);
                break;
            default:
                break;
        }

        return $jobResponse;
    }

    private function getKeywordBiddingKeywordPerformance(JobRequest $jobRequest): JobResponse
    {

        $jobResponse = new JobResponse();
        $startDate = $jobRequest->getParameters()->getStartDate();
        $endDate = $jobRequest->getParameters()->getEndDate();
        $country = $jobRequest->getCountry();
        $cookieString = $jobRequest->getCookie();

        /// validate
        ///

        $api = 'https://banhang.shopee.vn/api/marketing/v3/pas/report/search_report_by_time/?start_time=1624122000&end_time=1624208399&agg_interval=1&SPC_CDS=451d06e2-80b8-45e6-a58a-26ff99aee44c&SPC_CDS_VER=2';

        $headers = [
            'Cookie' => $cookieString
        ];
        $client = new Client();
        $data = $client->get($api, ['headers' => $headers]);

        var_dump($data);

        return $jobResponse;
    }


    private function getDomain($country)
    {
        $config = [
            'VN' => 'https://banhang.shopee.vn',
            'SG' => 'https://seller.shopee.sg'
        ];
        return $country[$country] ?? "";
    }
}